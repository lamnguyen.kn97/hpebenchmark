import torch
import torch.nn as nn
import torch.optim as optim
import torch.backends.cudnn as cudnn
import pandas as pd
import torchvision
import torchvision.transforms as transforms
from torchvision.models import resnet, densenet, vgg, squeezenet, inception
import os
import argparse
import time
# from models import *
from utils import progress_bar

parser = argparse.ArgumentParser(description='PyTorch CIFAR10 Training')
parser.add_argument('--lr', default=0.1, type=float, help='learning rate')
parser.add_argument('--resume', '-r', action='store_true', help='resume from checkpoint')
args = parser.parse_args()

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
best_acc = 0  # best test accuracy
start_epoch = 0  # start from epoch 0 or last checkpoint epoch

# Data
print('==> Preparing data..')
transform_train = transforms.Compose([
    transforms.RandomCrop(32, padding=4),
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
])

transform_test = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
])

trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform_train)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=32, shuffle=True, num_workers=2)

testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform_test)
testloader = torch.utils.data.DataLoader(testset, batch_size=100, shuffle=False, num_workers=2)

classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

# Model
print('==> Building model..')
# net = []
# net.append(VGG('VGG19'))
# net.append(ResNet18())
# net.append(PreActResNet18())
# net.append(GoogLeNet())
# net.append(DenseNet121())
# net.append(ResNeXt29_2x64d())
# net.append(MobileNet())
# net.append(MobileNetV2())
# net.append(DPN92())
# net.append(ShuffleNetG2())
# net.append(net = SENet18())
# net.append(ShuffleNetV2(1))


MODEL_LIST = {
    resnet: resnet.__all__[1:],
    densenet: densenet.__all__[1:],
    squeezenet: squeezenet.__all__[1:],
    vgg: vgg.__all__[5:]
}


# Training
def train(epoch, net, optimizer, criterion):
    print('\nEpoch: %d' % epoch)
    net.train()
    train_loss = 0
    correct = 0
    total = 0

    for batch_idx, (inputs, targets) in enumerate(trainloader):
        inputs, targets = inputs.to(device), targets.to(device)

        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, targets)
        loss.backward()
        optimizer.step()

        train_loss += loss.item()
        _, predicted = outputs.max(1)
        total += targets.size(0)
        correct += predicted.eq(targets).sum().item()

        progress_bar(batch_idx, len(trainloader), 'Loss: %.3f | Acc: %.3f%% (%d/%d)'
                     % (train_loss / (batch_idx + 1), 100. * correct / total, correct, total))

        return


def test(epoch, net, criterion):
    global best_acc
    net.eval()
    test_loss = 0
    correct = 0
    total = 0
    with torch.no_grad():
        for batch_idx, (inputs, targets) in enumerate(testloader):
            inputs, targets = inputs.to(device), targets.to(device)
            outputs = net(inputs)
            loss = criterion(outputs, targets)

            test_loss += loss.item()
            _, predicted = outputs.max(1)
            total += targets.size(0)
            correct += predicted.eq(targets).sum().item()

            progress_bar(batch_idx, len(testloader), 'Loss: %.3f | Acc: %.3f%% (%d/%d)'
                         % (test_loss / (batch_idx + 1), 100. * correct / total, correct, total))

    # Save checkpoint.
    acc = 100. * correct / total
    if acc > best_acc:
        print('Saving..')
        state = {
            'net': net.state_dict(),
            'acc': acc,
            'epoch': epoch,
        }
        if not os.path.isdir('checkpoint'):
            os.mkdir('checkpoint')
        torch.save(state, './checkpoint/ckpt.pth')
        best_acc = acc


stats = pd.DataFrame()
list_name = []
list_numgpu = []
list_benchmark = []
for model_type in MODEL_LIST.keys():
    for model_name in MODEL_LIST[model_type]:

        list_name.append(model_name)
        list_numgpu.append(os.environ['CUDA_VISIBLE_DEVICES'])
        model = getattr(model_type, model_name)(pretrained=False)
        model = torch.nn.DataParallel(model)
        model.cuda()
        criterion = nn.CrossEntropyLoss()
        optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=0.9, weight_decay=5e-4)
        torch.cuda.synchronize()
        begin = time.time()
        for epoch in range(start_epoch, start_epoch + 1):
            train(epoch, model, optimizer, criterion)
            test(epoch, model, criterion)
        torch.cuda.synchronize()
        end = time.time()
        print(f"Benchmark when train cifar-10 with {model_name}: {end - begin}")
        list_benchmark.append(end - begin)

n_gpu = len(os.environ['CUDA_VISIBLE_DEVICES'].split(','))
torch.cuda.synchronize()

stats['model_name'] = list_name
stats["num_gpu"] = list_numgpu
stats['benchmark'] = list_benchmark

print(stats.head())
stats.to_csv(f'./stat/pytorch_stats_{n_gpu}gpus.csv')